package javapractise;

import java.util.Scanner;

public class PalindromeCheck {

	static boolean palindromeCheck(int number) {
		
		int sum = 0,remainder,reverseOfNumber;
		int temporaryNumber = number;
	    boolean isPalindrome = false;
	    
		while (temporaryNumber != 0) {
			remainder = temporaryNumber % 10;
			sum = (sum * 10) + remainder;
			temporaryNumber = temporaryNumber / 10;
		}
		
		reverseOfNumber = sum;
		
		if (number == reverseOfNumber) {
			isPalindrome = true;
		}
		return isPalindrome;
	}

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number to check");
		int number = sc.nextInt();

		boolean isPalindrome = palindromeCheck(number);

		if (isPalindrome) {
			System.out.println("The number " + number + " is a palindrome number");
		} else {
			System.out.println("The number " + number + " is  not a palindrome number");
		}

	}

}

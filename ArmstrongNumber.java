package javapractise;

import java.util.ArrayList;
import java.util.Scanner;

public class ArmstrongNumber {

	static ArrayList<Integer> armstrongNumbersInRange(int start, int end) {

		//int arrayOfArmstrongNumbers[] = new int[end - start];
		ArrayList<Integer> arrayListOfArmstrongNumbers = new ArrayList<Integer>();
	//	int arrayIndex = 0;

		for (int i = start; i < end; i++) {
			int temp = i;
			int number = 0;

			while (temp != 0) {
				int rem = temp % 10;
				number = number + rem * rem * rem;
				temp = temp / 10;
			}
			if (i == number) {
				//arrayOfArmstrongNumbers[arrayIndex] = i;
				arrayListOfArmstrongNumbers.add(i);
				//arrayIndex++;
			}
		}
		return arrayListOfArmstrongNumbers;
	}

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the first number :");
		int start = sc.nextInt();
		System.out.println("Enter the second number :");
		int end = sc.nextInt();
		
		ArrayList<Integer> arrayListOfArmstrongNumbers=armstrongNumbersInRange(start,end);
		if(arrayListOfArmstrongNumbers.size () > 0) {
			System.out.println(arrayListOfArmstrongNumbers.size());
			System.out.println("the armstrong numbers between "+start+" and "+end+" are"+arrayListOfArmstrongNumbers);
		}
		else {
			System.out.println("no armstrong numbers are present");
		}

	}

}

package javapractise;

import java.util.Scanner;

public class LeapYearCheck {
	static boolean checkLeapYear(int year) {

		boolean isLeapYear;

		if (year % 4 == 0) {
			isLeapYear = true;
			if (year % 100 == 0) {
				if (year % 400 == 0) {
					isLeapYear = true;
				} else {
					isLeapYear = false;
				}
			}
		} else {
			isLeapYear = false;
		}

		return isLeapYear;
	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("enter the year to be checked");
		int year = sc.nextInt();

		boolean isLeapYear = checkLeapYear(year);

		if (isLeapYear) {
			System.out.println(year + " is a leap year");
		} else {
			System.out.println(year + " is not a leap year");
		}

	}

}

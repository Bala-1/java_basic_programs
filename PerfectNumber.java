package javapractise;

import java.util.Scanner;

public class PerfectNumber {

	static boolean perfectNumberCheck(int number) {

		int sumOfFactors = 0;

		for (int i = 1; i <= Math.sqrt(number); i++) {
			if (number % i == 0) {
				if (i == number / i) {
					sumOfFactors += i;
				} else {
					sumOfFactors += i + (number / i);
				}
			}
		}
		if (sumOfFactors - number == number) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number to check");
		int number = sc.nextInt();

		boolean isPerfectNumber = perfectNumberCheck(number);

		if (isPerfectNumber) {
			System.out.println("The number " + number + " is a perfect number");
		} else {
			System.out.println("The number " + number + " is  not a perfect number");
		}

	}
}

package javapractise;

import java.util.Scanner;

public class OddPalindromeCheckUsingPackage {

	static boolean oddPalindromeCheck(int number) {

		boolean ispalindrome = PalindromeCheck.palindromeCheck(number);
		boolean isOddPalindrome = false;

		if (ispalindrome) {

			isOddPalindrome = OddDigitCheck.oddDigitsCheck(number);
		}

		return isOddPalindrome;
	}

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number to check");
		int number = sc.nextInt();

		boolean isOddPalindrome = oddPalindromeCheck(number);

		if (isOddPalindrome) {
			System.out.println("The number " + number + " is an odd palindrome number");
		} else {
			System.out.println("The number " + number + " is not a odd palindrome number");
		}
	}
}

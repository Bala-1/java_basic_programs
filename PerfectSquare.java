package javapractise;

public class PerfectSquare {
	
	static boolean checkPerfectSquare(int num1, int num2) {
        int[] arr = new int[4];
        if (num1 == (num2 * num2)) {
            int k = 0, temp = num1;
            while (temp > 0) {
                arr[k] = temp % 10;
                temp = temp / 10;
                k++;
            }
            if (arr[0] % 2 == 0 && arr[1] % 2 == 0 && arr[2] % 2 == 0 && arr[3] % 2 == 0) {
                return true;
            }
        }
        return false;
    }
    public static void main(String args[]) {
        for (int i = 1000; i < 10000; i++) {
            for (int j = 32; j < 101; j++) {
                if (checkPerfectSquare(i, j)) {
                    System.out.println(i);
                }
            }
        }
    }

}

package javapractise;
import java.util.Scanner;
public class GreatestOf3Numbers {

	static int checkLargest(int firstNumber,int secondNumber,int thirdNumber ) {
		
		int largest = firstNumber;
		
		if(largest < secondNumber) {
			largest = secondNumber;
		}
		if(largest < thirdNumber) {
			largest = thirdNumber;
		}
		
		return largest;
	}

	public static void main(String[] args) {
		
		int firstNumber,secondNumber,thirdNumber,largestNumber;
		
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the first number");
		firstNumber = sc.nextInt();
		System.out.println("enter the second number");
		secondNumber = sc.nextInt();
		System.out.println("enter the third number");
		thirdNumber = sc.nextInt();
		
		largestNumber = checkLargest(firstNumber,secondNumber,thirdNumber);
		
		System.out.println("larget of the three numbers is "+largestNumber);

	}

}

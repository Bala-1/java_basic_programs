package javapractise;

import java.util.Scanner;

public class OddPalindromeCheck {

	static boolean palindromeCheck(int number) {

		int sum = 0, remainder, reverseOfNumber;
		int temporaryNumber = number;
		boolean isPalindrome = false;

		while (temporaryNumber != 0) {
			remainder = temporaryNumber % 10;
			sum = (sum * 10) + remainder;
			temporaryNumber = temporaryNumber / 10;
		}

		reverseOfNumber = sum;

		if (number == reverseOfNumber) {
			isPalindrome = true;
		}
		return isPalindrome;
	}

	public static boolean oddDigitsCheck(int n) {

		int temp = n;
		boolean isAllDigitsOdd = true;

		while (temp > 0) {

			int rem = temp % 10;
			if (rem % 2 == 0) {
				isAllDigitsOdd = false;
			}
			temp = temp / 10;
		}
		return isAllDigitsOdd;
	}
	
	static boolean oddPalindromeCheck(int number) {
		
		boolean ispalindrome = palindromeCheck(number);
		boolean isOddPalindrome = false;
		
		if(ispalindrome) {
			
			 isOddPalindrome = oddDigitsCheck(number);
		}
		
		return isOddPalindrome;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number to check");
		int number = sc.nextInt();

		boolean isOddPalindrome = oddPalindromeCheck(number);

		if (isOddPalindrome) {
			System.out.println("The number " + number + " is an odd palindrome number");
		} else {
			System.out.println("The number " + number + " is not a odd palindrome number");
		}

	}

}

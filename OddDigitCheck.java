package javapractise;

import java.util.Scanner;

public class OddDigitCheck {

	public static boolean oddDigitsCheck(int n) {

		int temp = n;
		boolean isAllDigitsOdd = true;

		while (temp > 0) {

			int rem = temp % 10;
			if (rem % 2 == 0) {
				isAllDigitsOdd = false;
			}
			temp = temp / 10;
		}
		return isAllDigitsOdd;
	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number to check for odd digits");
		int num = sc.nextInt();

		boolean isAllDigitsOdd = oddDigitsCheck(num);

		if (isAllDigitsOdd) {
			System.out.println("All digits in the number " + num + " are  odd");
		} else {
			System.out.println("All digits in the number " + num + " are not odd");
		}

	}

}

package javapractise;

import java.util.Scanner;

public class SumOfFactors {

	static int factorsSum(int number) {

		int sumOfFactors = 0;

		for (int i = 1; i <= Math.sqrt(number); i++) {
			if (number % i == 0) {
				if (i == number / i) {
					sumOfFactors += i;
				} else {
					sumOfFactors += i + (number /i);
				}
			}
		}
		return sumOfFactors;
	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number to find sum of all its factors");
		int num = sc.nextInt();

		System.out.println("sum of all factors of " + num + " is " + factorsSum(num));
		
	}

}
